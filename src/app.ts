type Config = {
    martrix: number[],
    level: number,
    dx: number,
    dy: number,
}

type imgList = {
    url: string,
    x: number,
    y: number,
    size: number
}

class ImageClass {
    readonly apiService: string = 'https://services.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/tile';
    // readonly apiService: string = 'src/full.png';
    private static instance: ImageClass;
    public imgList: imgList[];
    public images: HTMLImageElement[];
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    public level: number;

    constructor() {
        this.canvas = document.getElementById('map')! as HTMLCanvasElement;
        this.ctx = this.canvas.getContext('2d')! as CanvasRenderingContext2D;
        this.images = [];
        this.imgList = [];
        this.level = 0;

        this.calculateTileService(this.level)
        this.configure()
    }

    static getInstance() {
        if (this.instance) {
            return this.instance
        }
        this.instance = new ImageClass;
        return this.instance;
    }

    configure() {
        for (let i = 0; i < this.imgList.length; i++) {
            this.images[i] = new Image
            this.images[i].addEventListener('load', () => this.render(this.images[i], this.imgList[i]), false)
            this.images[i].src = this.imgList[i].url
        }
    }

    render(e, img) {
        // console.log(e, img)
        this.ctx.drawImage(e, img.x, img.y, img.size, img.size)
    }

    imageRender() {
        for (let i = 0; i < this.images.length; i++) {
            this.ctx.drawImage(this.images[i], this.imgList[i].x, this.imgList[i].y, this.imgList[i].size, this.imgList[i].size)
        }
    }

    clearImages() {
        this.imgList = [];
        this.images = [];
    }

    setImages(obj: any) {
        this.imgList.push({ url: obj.url, x: obj.x, y: obj.y, size: obj.size })
        // console.log(this.imgList)
    }

    calculateTileService(n: number) {
        this.clearImages()
        const zoom = n
        const num = Math.pow(4, zoom)
        const maximumLengthPerLine = num / 2;
        const row = Math.pow(2, zoom)
        const size = 800 / row;
        console.log(zoom, num, maximumLengthPerLine, row)
        if (zoom == 0) {
            this.setImages({
                url: `${this.apiService}/0/0/0`,
                // url: `${this.apiService}`,
                x: 0,
                y: 0,
                size: 800,
            })
        }
        // let html = '';

        for (let i = 0; i < row; i++) {
            for (let j = 0; j < row; j++) {
                this.setImages({
                    url: `${this.apiService}/${zoom}/${i}/${j}`,
                    // url: `${this.apiService}`,
                    x: j * size,
                    y: i * size,
                    size: size,
                })
            }
        }
    }

}

class DisplayTransform {
    private static instance: DisplayTransform;
    public canvas: HTMLCanvasElement;
    public ctx: CanvasRenderingContext2D;
    public config: Config

    private constructor() {
        this.canvas = document.getElementById('map')! as HTMLCanvasElement;
        this.ctx = this.canvas.getContext('2d')
        this.config = {
            martrix: [1, 0, 0, 1, 0, 0],
            level: 0,
            dx: 0,
            dy: 0,
        }
    }

    static getInstance() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new DisplayTransform;
        return this.instance;
    }

    setMove(m: string) {
        // let [a, b, c, d, e, f] = this.config.martrix
        const flag = 50;
        if (m == "up") {
            this.config.martrix[5] += -flag
            this.config.dy += -flag
        } else if (m == "down") {
            this.config.martrix[5] += flag
            this.config.dy += flag
        } else if (m == "left") {
            this.config.martrix[4] += -flag
            this.config.dx += -flag
        } else if (m == "right") {
            this.config.martrix[4] += flag
            this.config.dx += flag
        }

    }

    setMartrix(n: number) {
        let i = this.config.martrix[0]

        if (n > 0) {
            i += 1

        } else {
            i -= 1
        }

        if (i < 1) {
            console.log('limit')
            return;
        }

        // if (i > 2 || i < 1) {
        //     return
        // }

        let x = -(i - 1) * this.canvas.width / 2
        let y = -(i - 1) * this.canvas.height / 2
        this.config.martrix = [i, 0, 0, i, x + this.config.dx, y + this.config.dy]
    }

    setTransform() {
        let i = 0;
        this.ctx.setTransform(this.config.martrix[0], this.config.martrix[1], this.config.martrix[2], this.config.martrix[3], this.config.martrix[4], this.config.martrix[5])
        // this.ctx.translate(300, 300)
        // this.ctx.setTransform(1, 0, 0, 1, 0, 0)
    }

    setHome() {
        let i = this.config.martrix[0]
        this.config.martrix = [this.config.martrix[0], 0, 0, this.config.martrix[3], 0, 0];
        // this.ctx.setTransform(this.config.martrix[0], 0, 0, this.config.martrix[3], 0, 0)
        this.ctx.setTransform(this.config.martrix[0], 0, 0, this.config.martrix[3], 0, 0)
    }

    clear() {
        this.ctx.clearRect(0, 0, 800, 800)
    }

    update() {
        this.ctx.setTransform(this.config.martrix[0], this.config.martrix[1], this.config.martrix[2], this.config.martrix[3], this.config.martrix[4], this.config.martrix[5])
    }
}

class ProjectMap {
    btnZoomin: HTMLButtonElement;
    btnZoomout: HTMLButtonElement;
    btnReset: HTMLButtonElement;
    // btnUp: HTMLButtonElement;
    // btnDown: HTMLButtonElement;

    moveUp: HTMLButtonElement;
    moveDown: HTMLButtonElement;
    moveLeft: HTMLButtonElement;
    moveRight: HTMLButtonElement;

    canvas: HTMLCanvasElement;
    labelNumImg: HTMLSpanElement;
    list: HTMLUListElement;

    ctx: any;
    countInit: number;
    zoomScale: number;

    constructor() {
        this.btnZoomin = document.getElementById('btn-in')! as HTMLButtonElement;
        this.btnZoomout = document.getElementById('btn-out')! as HTMLButtonElement;
        this.btnReset = document.getElementById('btn-reset')! as HTMLButtonElement;
        // this.btnUp = document.getElementById('btn-up')! as HTMLButtonElement;
        // this.btnDown = document.getElementById('btn-down')! as HTMLButtonElement;
        this.canvas = document.getElementById('map')! as HTMLCanvasElement;
        this.labelNumImg = document.getElementById('num-img')! as HTMLSpanElement;

        this.moveUp = document.getElementById('move-up')! as HTMLButtonElement;
        this.moveDown = document.getElementById('move-down')! as HTMLButtonElement;
        this.moveLeft = document.getElementById('move-left')! as HTMLButtonElement;
        this.moveRight = document.getElementById('move-right')! as HTMLButtonElement;

        this.list = document.getElementById('or-img-download')! as HTMLUListElement;

        this.countInit = 0;
        this.zoomScale = 0;
        this.ctx = this.canvas.getContext('2d')

        this.configure()
    }

    btnHandler(e: Event) {
        const target = e.target! as HTMLInputElement;
        displayTransform.clear()
        // imageClass.clearImages()
        if (target.value == 'in') {
            displayTransform.setMartrix(1)
            this.zoomScale += 1;
            this.up()
        } else {
            displayTransform.setMartrix(-1)
            this.zoomScale -= 1;
            this.down()
        }
        this.update()
    }

    up() {
        if (this.zoomScale % 3 == 0) {
            this.countInit = this.countInit + 1
            imageClass.calculateTileService(this.countInit)
            // this.update();
            imageClass.configure()

            this.zoomScale = 0;
            this.labelNumImg.innerHTML = Math.pow(4, this.countInit).toString();
        }
    }

    down() {
        if (this.zoomScale < 0) {
            let temp = this.zoomScale * -1
            if (temp % 3 == 0) {

                this.countInit = this.countInit - 1
                if (this.countInit < 0) {
                    this.countInit = 0;
                    // return;
                }
                imageClass.calculateTileService(this.countInit)
                // this.update();
                imageClass.configure()

                this.labelNumImg.innerHTML = Math.pow(4, this.countInit).toString();
            }
        }

    }

    Home() {
        console.log('home btn')
        displayTransform.clear()
        displayTransform.setHome()
        imageClass.imageRender()
        // this.update()
    }

    move(e: Event) {
        let target = e.target! as HTMLInputElement;
        displayTransform.setMove(target.value);
        this.update()
    }

    configure() {
        this.btnZoomin.addEventListener('click', this.btnHandler.bind(this))
        this.btnZoomout.addEventListener('click', this.btnHandler.bind(this))
        this.btnReset.addEventListener('click', this.Home.bind(this))
        // this.btnUp.addEventListener('click', this.up.bind(this))
        // this.btnDown.addEventListener('click', this.down.bind(this))

        this.moveUp.addEventListener('click', this.move.bind(this))
        this.moveDown.addEventListener('click', this.move.bind(this))
        this.moveLeft.addEventListener('click', this.move.bind(this))
        this.moveRight.addEventListener('click', this.move.bind(this))

        // this.labelNumImg.innerText = '0';

        this.canvas.width = 800;
        this.canvas.height = 800;

        this.DOMRender()
    }

    DOMRender() {
        let num = Math.pow(4, this.countInit).toString();
        let html = '';
        this.labelNumImg.innerHTML = (num == "0") ? "1" : num;

        if (imageClass.imgList.length == 2) {
            html += `<li>${imageClass.imgList[0].url}</li>`
            this.list.innerHTML = html;
            return;
        }
        // console.log(imageClass.imgList)
        imageClass.imgList.map(img => {
            html += `<li>${img.url}</li>`
        })
        this.list.innerHTML = html;
    }


    update() {
        // console.log('update')
        displayTransform.clear()
        displayTransform.setTransform()
        imageClass.imageRender()

        this.DOMRender()
    }
}

const imageClass = ImageClass.getInstance();
const displayTransform = DisplayTransform.getInstance();
// const state = State.getInstance();
const proj = new ProjectMap();
