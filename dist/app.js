class ImageClass {
    constructor() {
        this.apiService = 'https://services.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/tile';
        this.canvas = document.getElementById('map');
        this.ctx = this.canvas.getContext('2d');
        this.images = [];
        this.imgList = [];
        this.level = 0;
        this.calculateTileService(this.level);
        this.configure();
    }
    static getInstance() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new ImageClass;
        return this.instance;
    }
    configure() {
        for (let i = 0; i < this.imgList.length; i++) {
            this.images[i] = new Image;
            this.images[i].addEventListener('load', () => this.render(this.images[i], this.imgList[i]), false);
            this.images[i].src = this.imgList[i].url;
        }
    }
    render(e, img) {
        this.ctx.drawImage(e, img.x, img.y, img.size, img.size);
    }
    imageRender() {
        for (let i = 0; i < this.images.length; i++) {
            this.ctx.drawImage(this.images[i], this.imgList[i].x, this.imgList[i].y, this.imgList[i].size, this.imgList[i].size);
        }
    }
    clearImages() {
        this.imgList = [];
        this.images = [];
    }
    setImages(obj) {
        this.imgList.push({ url: obj.url, x: obj.x, y: obj.y, size: obj.size });
    }
    calculateTileService(n) {
        this.clearImages();
        const zoom = n;
        const num = Math.pow(4, zoom);
        const maximumLengthPerLine = num / 2;
        const row = Math.pow(2, zoom);
        const size = 800 / row;
        console.log(zoom, num, maximumLengthPerLine, row);
        if (zoom == 0) {
            this.setImages({
                url: `${this.apiService}/0/0/0`,
                x: 0,
                y: 0,
                size: 800,
            });
        }
        for (let i = 0; i < row; i++) {
            for (let j = 0; j < row; j++) {
                this.setImages({
                    url: `${this.apiService}/${zoom}/${i}/${j}`,
                    x: j * size,
                    y: i * size,
                    size: size,
                });
            }
        }
    }
}
class DisplayTransform {
    constructor() {
        this.canvas = document.getElementById('map');
        this.ctx = this.canvas.getContext('2d');
        this.config = {
            martrix: [1, 0, 0, 1, 0, 0],
            level: 0,
            dx: 0,
            dy: 0,
        };
    }
    static getInstance() {
        if (this.instance) {
            return this.instance;
        }
        this.instance = new DisplayTransform;
        return this.instance;
    }
    setMove(m) {
        const flag = 50;
        if (m == "up") {
            this.config.martrix[5] += -flag;
            this.config.dy += -flag;
        }
        else if (m == "down") {
            this.config.martrix[5] += flag;
            this.config.dy += flag;
        }
        else if (m == "left") {
            this.config.martrix[4] += -flag;
            this.config.dx += -flag;
        }
        else if (m == "right") {
            this.config.martrix[4] += flag;
            this.config.dx += flag;
        }
    }
    setMartrix(n) {
        let i = this.config.martrix[0];
        if (n > 0) {
            i += 1;
        }
        else {
            i -= 1;
        }
        if (i < 1) {
            console.log('limit');
            return;
        }
        let x = -(i - 1) * this.canvas.width / 2;
        let y = -(i - 1) * this.canvas.height / 2;
        this.config.martrix = [i, 0, 0, i, x + this.config.dx, y + this.config.dy];
    }
    setTransform() {
        let i = 0;
        this.ctx.setTransform(this.config.martrix[0], this.config.martrix[1], this.config.martrix[2], this.config.martrix[3], this.config.martrix[4], this.config.martrix[5]);
    }
    setHome() {
        let i = this.config.martrix[0];
        this.config.martrix = [this.config.martrix[0], 0, 0, this.config.martrix[3], 0, 0];
        this.ctx.setTransform(this.config.martrix[0], 0, 0, this.config.martrix[3], 0, 0);
    }
    clear() {
        this.ctx.clearRect(0, 0, 800, 800);
    }
    update() {
        this.ctx.setTransform(this.config.martrix[0], this.config.martrix[1], this.config.martrix[2], this.config.martrix[3], this.config.martrix[4], this.config.martrix[5]);
    }
}
class ProjectMap {
    constructor() {
        this.btnZoomin = document.getElementById('btn-in');
        this.btnZoomout = document.getElementById('btn-out');
        this.btnReset = document.getElementById('btn-reset');
        this.canvas = document.getElementById('map');
        this.labelNumImg = document.getElementById('num-img');
        this.moveUp = document.getElementById('move-up');
        this.moveDown = document.getElementById('move-down');
        this.moveLeft = document.getElementById('move-left');
        this.moveRight = document.getElementById('move-right');
        this.list = document.getElementById('or-img-download');
        this.countInit = 0;
        this.zoomScale = 0;
        this.ctx = this.canvas.getContext('2d');
        this.configure();
    }
    btnHandler(e) {
        const target = e.target;
        displayTransform.clear();
        if (target.value == 'in') {
            displayTransform.setMartrix(1);
            this.zoomScale += 1;
            this.up();
        }
        else {
            displayTransform.setMartrix(-1);
            this.zoomScale -= 1;
            this.down();
        }
        this.update();
    }
    up() {
        if (this.zoomScale % 3 == 0) {
            this.countInit = this.countInit + 1;
            imageClass.calculateTileService(this.countInit);
            imageClass.configure();
            this.zoomScale = 0;
            this.labelNumImg.innerHTML = Math.pow(4, this.countInit).toString();
        }
    }
    down() {
        if (this.zoomScale < 0) {
            let temp = this.zoomScale * -1;
            if (temp % 3 == 0) {
                this.countInit = this.countInit - 1;
                if (this.countInit < 0) {
                    this.countInit = 0;
                }
                imageClass.calculateTileService(this.countInit);
                imageClass.configure();
                this.labelNumImg.innerHTML = Math.pow(4, this.countInit).toString();
            }
        }
    }
    Home() {
        console.log('home btn');
        displayTransform.clear();
        displayTransform.setHome();
        imageClass.imageRender();
    }
    move(e) {
        let target = e.target;
        displayTransform.setMove(target.value);
        this.update();
    }
    configure() {
        this.btnZoomin.addEventListener('click', this.btnHandler.bind(this));
        this.btnZoomout.addEventListener('click', this.btnHandler.bind(this));
        this.btnReset.addEventListener('click', this.Home.bind(this));
        this.moveUp.addEventListener('click', this.move.bind(this));
        this.moveDown.addEventListener('click', this.move.bind(this));
        this.moveLeft.addEventListener('click', this.move.bind(this));
        this.moveRight.addEventListener('click', this.move.bind(this));
        this.canvas.width = 800;
        this.canvas.height = 800;
        this.DOMRender();
    }
    DOMRender() {
        let num = Math.pow(4, this.countInit).toString();
        let html = '';
        this.labelNumImg.innerHTML = (num == "0") ? "1" : num;
        if (imageClass.imgList.length == 2) {
            html += `<li>${imageClass.imgList[0].url}</li>`;
            this.list.innerHTML = html;
            return;
        }
        imageClass.imgList.map(img => {
            html += `<li>${img.url}</li>`;
        });
        this.list.innerHTML = html;
    }
    update() {
        displayTransform.clear();
        displayTransform.setTransform();
        imageClass.imageRender();
        this.DOMRender();
    }
}
const imageClass = ImageClass.getInstance();
const displayTransform = DisplayTransform.getInstance();
const proj = new ProjectMap();
//# sourceMappingURL=app.js.map